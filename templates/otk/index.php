<?php
defined('_JEXEC') or die('Restricted access');
include 'lib/head.php';
?>
<!doctype html>
<html lang="<?=$this->language?>">

<head>
<jdoc:include type="head" />
</head>

<body<?=(isset($pageclass) ? ' class="'.$pageclass.'" ' : '')?>>

<header class="header">
    <div class="banner">
        <div class="logo">
            <div class="logo-type">
                <a class="logo-link" href="">
                    <span><?=JText::_('LOGO_TYPE_LINE1')?></span>
                    <span><?=JText::_('LOGO_TYPE_LINE2')?></span>
                </a>
            </div>
            <p class="logo-slogan">
                <span><?=JText::_('LOGO_TYPE_SLOGAN')?></span>
            </p>
        </div>
        <? /* TODO: make the verse citation a dynamic module and don't serve it to bots */ ?>
        <figure class="verse">
            <? /* <jdoc:include type="modules" name="verse" /> */ ?>
            <figcaption class="verse-caption"><?=JText::_('VERSE_TITLE')?></figcaption>
            <blockquote class="verse-quote">
                <div class="verse-text">
                    <p>Мне придётся разделить человечество на людей Истинной ВЕРЫ и людей НЕВЕРИЯ, ибо первые будут ТВОРИТЬ вместе со Мной на Планете Святая Русь Божественную монархию, как образец абсолютного СОВЕРШЕНСТВА для всего Великого КОСМОСА, для всех форм проявления Разумной жизни, а вторые будут вынуждены перебраться на другую планету и продолжить там эволюцию своего Со-Знания в Моём КАНАЛЕ и под Моим постоянным контролем!</p>
                </div>
                <footer class="verse-details">
                    <cite class="verse-source">
                        (<a class="verse-link" href=""><?=JText::_('VERSE_CITATION_FROM')?>
                        <span class="verse-date">15.06.14</span></a>,
                        <?=JText::_('VERSE_VERSE')?>
                        <span class="verse-number">17</span>)
                    </cite>
                </footer>
            </blockquote>
        </figure>
    </div>
    <nav class="nav">
        <jdoc:include type="modules" name="nav" />
    </nav>
</header>

<main class="main">
    <jdoc:include type="message" />
    <div class="sidebar">
        <jdoc:include type="modules" name="sidebar" style="sidebarItem" />
    </div>
    <div class="main-content">
        <jdoc:include type="component" />
    </div>
</main>

<footer class="footer">
    <div class="copyright">
        <p>Copyright &copy; <a href="mailto:maslovli@yandex.ru">Leonid Maslov</a> 2004-<?=date('Y')?></p>
    </div>
    <div class="copy-protection">
        <p><?=JText::sprintf('COPYING_REQUIRES_BACKLINK', '<a href="http://www.otkroveniya.info/">', '</a>')?></p>
    </div>
    <div class="badges">
        <jdoc:include type="modules" name="badges" />
    </div>
</footer>

<jdoc:include type="modules" name="debug" />

</body>

</html>
