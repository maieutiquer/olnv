<?php

defined('_JEXEC') or die;

/**
 * sidebarItem chrome.
 *
 * @since   3.0
 */
function modChrome_sidebarItem($module, &$params, &$attribs)
{
	$headerLevel = isset($attribs['headerLevel']) ? (int) $attribs['headerLevel'] : 3;
	if (!empty ($module->content)) { ?>
<div class="sidebar-item <?=htmlspecialchars($params->get('moduleclass_sfx'));?>">
<?php if ($module->showtitle) { ?>
    <h<?=$headerLevel;?>><?=$module->title;?></h<?=$headerLevel;?>>
<?php }; ?>
    <div class="sidebar-item-content">
        <?=$module->content;?>
    </div>
</div>
<?php };
}
