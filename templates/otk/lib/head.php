<?php
defined('_JEXEC') or die('Restricted access');


$doc = JFactory::getDocument();
$app = JFactory::getApplication();

$this->setGenerator(null);
$this->setTitle($app->getCfg('sitename').' :: '.$this->getTitle());
$this->setMetaData('X-UA-Compatible', 'IE=edge', true);
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery-noconflict.js']);
unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery-migrate.min.js']);
// unset($this->_scripts[JURI::root(true).'/media/jui/js/bootstrap.min.js']);

// $this->addScript('templates/'.$this->template.'/js/script.js');
$this->addStylesheet('templates/'.$this->template.'/css/style.css');

$menu = $app->getMenu()->getActive();
$pageclass = ''; // Notice how the variable is empty first. Then place the code below.

if (is_object($menu)) {
    $pageclass = $menu->params->get('pageclass_sfx');
}
