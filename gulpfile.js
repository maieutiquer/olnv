var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer-core');
var mqpacker = require('css-mqpacker');

var templatePath = 'templates/otk'

gulp.task('sass', function() {
    var processors = [
        autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}),
        mqpacker,
    ];
    gulp.src(templatePath + '/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({sourcemap: true, outputStyle: 'compact'})
            .on('error', sass.logError))
        // .pipe(postcss(processors))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(templatePath + '/css'));
});

//Watch task
gulp.task('default',function() {
    gulp.watch('templates/otk/scss/**/*.scss',['sass']);
});
